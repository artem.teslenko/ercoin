# Licensed under the Apache License, Version 2.0 (the “License”);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an “AS IS” BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PROJECT = ercoin
PROJECT_DESCRIPTION = A simple cryptocurrency using Tendermint
PROJECT_VERSION = 1.0.1

DEPS = abci_server datum dynarec elixir enacl erlsha2 gb_merkle_trees jiffy nist_beacon rational
dep_abci_server = git https://github.com/KrzysiekJ/abci_server.git v0.12.1
dep_datum = git https://github.com/fogfish/datum.git 3186018
dep_dynarec = git https://github.com/dieswaytoofast/dynarec.git 1f477
dep_elixir = git https://github.com/elixir-lang/elixir.git v1.7.3
dep_enacl = git https://github.com/jlouis/enacl.git f650c72b028e46dbbed35f94e33ebe1e8db5d7eb
dep_erlsha2 = git https://github.com/vinoski/erlsha2 e3434b33cfeea02609bbf877954d856d895b9e1d
dep_gb_merkle_trees = git https://github.com/KrzysiekJ/gb_merkle_trees.git 1687f8be1187cf0964e63012b175b286af69c21a
dep_jiffy = git https://github.com/davisp/jiffy.git 0.14.11
dep_nist_beacon = git https://gitlab.com/KrzysiekJ/nist_beacon v0.2.1
dep_rational = git https://github.com/shortishly/erlang-rational.git 404694

LOCAL_DEPS = crypto

TEST_DEPS = triq
dep_triq = git https://gitlab.com/triq/triq.git 4e92ea85

BUILD_DEPS = lfe lfe.mk
dep_lfe = git https://github.com/rvirding/lfe 2880c8a2
dep_lfe.mk = git https://github.com/KrzysiekJ/lfe.mk 025e92038f330ae2b0cbfe170819419c70118ad8
DEP_PLUGINS = lfe.mk

NO_AUTOPATCH = elixir

# Whitespace to be used when creating files from templates.
SP = 4

# This is to treat warnings as errors in when running make check, see https://github.com/ninenines/erlang.mk/issues/809 .
check:: app

include erlang.mk

$(DIALYZER_PLT): test-deps
DIALYZER_DIRS += -r $(TEST_DIR)
# eunit is mentioned explicitly as a workaround until https://github.com/ninenines/erlang.mk/issues/770 is fixed.
PLT_APPS = $(TEST_DEPS) eunit

# A workaround until https://github.com/ninenines/lfe.mk/issues/3 is fixed.
deps:: $(DEPS_DIR)/lfe/ebin/clj.beam
$(DEPS_DIR)/lfe/ebin/clj.beam:
	 $(verbose) PATH=$(PATH):$(DEPS_DIR)/lfe/bin lfec +debug_info -o $(DEPS_DIR)/lfe/ebin/ $(DEPS_DIR)/lfe/src/clj.lfe

ebin/$(PROJECT).app:: no-bad-mix-app-files | $(DEPS_DIR)/elixir/ebin

$(DEPS_DIR)/elixir/ebin:
	$(gen_verbose) ln -s lib/elixir/ebin $(DEPS_DIR)/elixir/

no-bad-mix-app-files:
	$(verbose) rm -rf $(DEPS_DIR)/elixir/lib/mix/test

filter-genesis-txs:
	@$(SHELL_ERL) -pa $(SHELL_PATHS) -noshell -eval 'ercoin_genesis:filter_genesis_txs(), halt()'

ifeq ($(shell which sha256 2> /dev/null),)
# GNU
sha256_check = sha256sum $(1) | cut -f 1 -d\  | head -c -1 | xargs test $(2) =
else
# BSD
sha256_check = sha256 -q -c $(2) $(1)
endif

ERCOIN_HOME ?= $(HOME)/.ercoin
ERCOIN_CONFIG = $(ERCOIN_HOME)/config

# The order of dependencies is important: the public key of validator may be used to generate genesis data.
# The presence of priv_validator_key.json is however not required, so we cannot make it a rigid dependency.
init: $(ERCOIN_CONFIG)/priv_validator_key.json $(ERCOIN_CONFIG)/config.toml.patched $(ERCOIN_CONFIG)/genesis.json.custom

# This dependency is added to ensure that our genesis.json overwrites the Tendermint-generated one, not vice-versa.
$(ERCOIN_CONFIG)/genesis.json.custom: $(ERCOIN_CONFIG)/genesis.json

CHAIN_ID ?= ercoin-test
ifeq ($(STOOL_FILE),)
GENESIS_GENERATOR = ercoin_genesis:testnet_data($(data_opts))
else
GENESIS_GENERATOR = ercoin_genesis:initial_data(\"$(STOOL_FILE)\")
endif
@echo $(GENESIS_GENERATOR)

# We don’t put app as a dependency here because standard input is then eaten.
$(ERCOIN_CONFIG)/genesis.json.custom:
	$(verbose) mkdir -p $(ERCOIN_CONFIG)
ifeq ($(GENESIS_URL),)
	$(verbose) $(SHELL_ERL) -pa $(SHELL_PATHS) -noshell -eval\
		"application:ensure_all_started(nist_beacon),\
		file:write_file(\"$(ERCOIN_CONFIG)/genesis.json\", ercoin_genesis:data_to_genesis_json($(GENESIS_GENERATOR), <<\"$(CHAIN_ID)\">>)),\
		halt()"
else
	$(verbose) curl -so $(ERCOIN_CONFIG)/genesis.json $(GENESIS_URL)
ifeq ($(GENESIS_SHA256),)
	$(warning GENESIS_SHA256 not specified, skipping a check…)
else
	$(verbose) $(call sha256_check,$(ERCOIN_CONFIG)/genesis.json,$(GENESIS_SHA256))
endif
endif
	$(gen_verbose) touch $@

$(ERCOIN_CONFIG) $(ERCOIN_CONFIG)/priv_validator_key.json $(ERCOIN_CONFIG)/config.toml:
	$(gen_verbose) tendermint init --home $(ERCOIN_HOME)

$(ERCOIN_CONFIG)/config.toml.patched: $(ERCOIN_CONFIG)/config.toml
	$(verbose) sed -f config.toml.sed $< > $<.tmp
	$(verbose) mv $<.tmp $<
	$(gen_verbose) touch $@

.PHONY: filter-genesis-txs no-bad-mix-app-files init
