%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_abci_tests).

-include_lib("abci_server/include/abci.hrl").
-include_lib("include/ercoin_test.hrl").

-compile({parse_transform, dynarec}).

-spec end_block(data()) -> data().
end_block(Data) ->
    {keep_state, NewData, {reply, "from", #'tendermint.abci.types.ResponseEndBlock'{}}} =
        ercoin_abci:handle_event({call, "from"}, #'tendermint.abci.types.RequestEndBlock'{}, committing, Data),
    NewData.

-spec apply_begin_block(#'tendermint.abci.types.RequestBeginBlock'{}, data()) -> data().
apply_begin_block(BeginBlock, Data) ->
    {next_state,
     committing,
     NewData,
     {reply, "from", #'tendermint.abci.types.ResponseBeginBlock'{}}} =
        ercoin_abci:handle_event({call, "from"}, BeginBlock, gossiping, Data),
    NewData.

-spec apply_diffs(list(#'tendermint.abci.types.ValidatorUpdate'{}), gb_merkle_trees:tree()) -> gb_merkle_trees:tree().
apply_diffs([], Validators) ->
    Validators;
apply_diffs([#'tendermint.abci.types.ValidatorUpdate'{power=VP, pub_key=#'tendermint.abci.types.PubKey'{data=PubKey, type="ed25519"}}|Diffs], Validators) ->
    NewValidators =
        case VP of
            0 ->
                gb_merkle_trees:delete(PubKey, Validators);
            _ ->
                RestBin =
                    case gb_merkle_trees:lookup(PubKey, Validators) of
                        <<_:1/unit:8, RestBin2/binary>> ->
                            RestBin2;
                        none ->
                            <<0:3/unit:8>>
                    end,
                gb_merkle_trees:enter(PubKey, <<VP, RestBin/binary>>, Validators)
        end,
    apply_diffs(Diffs, NewValidators).

-spec data_to_app_state_bytes(data()) -> binary().
data_to_app_state_bytes(Data) ->
    iolist_to_binary(io_lib:format("\"~s\"", [base64:encode(term_to_binary(Data))])).

prop_init_chain_sets_data_and_responds_with_validators() ->
    ?FORALL(
       Data=#data{validators=Validators},
       data(),
       begin
           {next_state, gossiping, ResponseData, {reply, foo, #'tendermint.abci.types.ResponseInitChain'{validators=ResponseValidators}}} =
               ercoin_abci:handle_event(
                 {call, foo},
                 #'tendermint.abci.types.RequestInitChain'{
                    app_state_bytes=data_to_app_state_bytes(Data)},
                 uninitialized,
                 none),
           ResponseData =:= Data andalso
               ResponseValidators =:= [#'tendermint.abci.types.ValidatorUpdate'{power=Power, pub_key=#'tendermint.abci.types.PubKey'{data=PK, type="ed25519"}} || {PK, <<Power, _/binary>>} <- gb_merkle_trees:to_orddict(Validators)]
       end).

prop_future_validators_announcement() ->
    ?FORALL(
       Data=#data{epoch_stage=EpochStage, validators=Validators, future_validators=FutureValidators},
       data(),
       begin
           {keep_state,
            _,
            {reply, "from", #'tendermint.abci.types.ResponseEndBlock'{validator_updates=Diffs}}} =
               ercoin_abci:handle_event({call, "from"}, #'tendermint.abci.types.RequestEndBlock'{}, committing, Data),
           NormalizationFun =
               fun (Validators2) ->
                       gb_merkle_trees:foldr(
                         fun ({PK, <<Power, _/binary>>}, Acc) ->
                                 [{PK, Power}|Acc]
                         end,
                         [],
                         Validators2)
               end,
           case EpochStage of
               drawing_to_be_announced ->
                   NormalizationFun(apply_diffs(Diffs, Validators)) =:= NormalizationFun(FutureValidators);
               _ ->
                   Diffs =:= []
           end
       end).

prop_end_block_increments_height() ->
    %% We could increment height in BeginBlock, but this would mean that, when validating transactions, we would operate under different state when gossiping and when commiting.
    %% Incrementing height at EndBlock means that in many operations (like in validating transactions) we need to add 1 to height.
    ?FORALL(
       Data,
       data(),
       get_value(height, end_block(Data)) =:= get_value(height, Data) + 1).

prop_deliver_tx_handles_binary_in_regard_to_data() ->
    ?FORALL(
       {Data, MaybeTxBin},
       ercoin_tx_gen:data_with_maybe_tx_bin(),
       begin
           {keep_state, NewData, {reply, "from", #'tendermint.abci.types.ResponseDeliverTx'{code=ErrorCode}}} =
               ercoin_abci:handle_event(
                 {call, "from"},
                  #'tendermint.abci.types.RequestDeliverTx'{
                     tx=MaybeTxBin},
                  committing,
                  Data),
           {ErrorCode, NewData} =:= ercoin_tx:handle_bin(MaybeTxBin, Data)
       end).

prop_deliver_tx_sets_events_for_successful_transactions() ->
    ?FORALL(
       {Data, TxBin},
       ercoin_tx_gen:data_with_tx_bin(),
       begin
           {keep_state, _, {reply, "from", #'tendermint.abci.types.ResponseDeliverTx'{code=?OK, events=Events}}} =
               ercoin_abci:handle_event(
                 {call, "from"},
                  #'tendermint.abci.types.RequestDeliverTx'{
                     tx=TxBin},
                  committing,
                  Data),
           Events =:=
               [ercoin_event:to_abci(Event) || Event <- ercoin_tx:events(ercoin_tx:deserialize(TxBin), Data)]
       end).

%% TODO: Slash validators that propose invalid transactions.

%% A transaction may have a higher timestamp than the timestamp of last block created, so we want to check it against current timestamp, not the timestamp of last block.
%% On the other hand, when a block is created, it may have higher timestamp than the current timestamp, so a transaction that is valid now may become invalid in the block. Therefore we want to impose lower age margin in CheckTx.
prop_check_tx_handles_binary_in_regard_to_mempool_data_with_current_timestamp_and_lower_age_margin() ->
    ?FORALL(
       {{MempoolData, MaybeTxBin},
        CheckTxType,
        StateName},
       {ercoin_tx_gen:data_with_maybe_tx_bin(),
        oneof(['New', 'Recheck']),
        oneof([gossiping, committing])},
       begin
           {keep_state, #data{mempool_data=NewMempoolData}, {reply, "from", #'tendermint.abci.types.ResponseCheckTx'{code=ErrorCode}}} =
               ercoin_abci:handle_event(
                 {call, "from"},
                  #'tendermint.abci.types.RequestCheckTx'{
                     tx=MaybeTxBin,
                     type=CheckTxType},
                  StateName,
                  #data{mempool_data=MempoolData}),
           {ErrorCode, NewMempoolData} =:=
               ercoin_tx:handle_bin(
                 MaybeTxBin,
                 ercoin_data:apply_shift_to_now(MempoolData),
                 ercoin_tx:default_age_margin(MempoolData) * 3 div 4)
       end).

prop_tx_puts_fee_into_fee_deposit() ->
    ?FORALL(
       {Data, Tx},
       ercoin_tx_gen:data_with_tx(),
       begin
           #data{fee_deposit=FeeDeposit} = Data,
           Fee = ercoin_fee:fee(Tx, Data),
           {keep_state,
            #data{fee_deposit=NewFeeDeposit},
            {reply, "from", #'tendermint.abci.types.ResponseDeliverTx'{code=?OK}}} =
               ercoin_abci:handle_event(
                 {call, "from"},
                 #'tendermint.abci.types.RequestDeliverTx'{
                    tx=ercoin_tx:serialize(Tx)},
                 committing,
                 Data),
           NewFeeDeposit =:= FeeDeposit + Fee
       end).

prop_info_responds_with_app_hash_and_height_when_gossiping() ->
    ?FORALL(
       Data=#data{height=Height},
       data(),
       begin
           {keep_state_and_data, {reply, "from", #'tendermint.abci.types.ResponseInfo'{last_block_height=ResponseHeight, last_block_app_hash=AppHash}}} =
               ercoin_abci:handle_event({call, "from"}, #'tendermint.abci.types.RequestInfo'{}, gossiping, Data),
           ResponseHeight =:= Height andalso AppHash =:= ercoin_data:app_hash(Data)
       end).

prop_commit_responds_with_app_hash() ->
    ?FORALL(
       Data,
       data(),
       begin
           {next_state, gossiping, NewData, [{reply, "from", #'tendermint.abci.types.ResponseCommit'{data=ResponseData}}|_]} =
               ercoin_abci:handle_event({call, "from"}, #'tendermint.abci.types.RequestCommit'{}, committing, Data),
           ResponseData =:= ercoin_data:app_hash(NewData)
       end).

prop_begin_block_updates_timestamps() ->
    ?FORALL(
       {Data, BeginBlock},
       ercoin_gen:data_with_begin_block(),
       begin
           #data{timestamp=NewTimestamp, previous_timestamp=NewPreviousTimestamp} = apply_begin_block(BeginBlock, Data),
           NewTimestamp =:= ercoin_timestamp:from_protobuf(BeginBlock#'tendermint.abci.types.RequestBeginBlock'.header#'tendermint.abci.types.Header'.time) andalso
               NewPreviousTimestamp =:= Data#data.timestamp
       end).

prop_begin_block_responds_with_events() ->
    ?FORALL(
       {Data, BeginBlock},
       ercoin_gen:data_with_begin_block(),
       begin
           {next_state,
            committing,
            _,
            {reply, "from", #'tendermint.abci.types.ResponseBeginBlock'{events=ABCIEvents}}} =
               ercoin_abci:handle_event({call, "from"}, BeginBlock, gossiping, Data),
           {_, Events} =
               ercoin_data:shift_to_timestamp(
                 ercoin_timestamp:from_protobuf(BeginBlock#'tendermint.abci.types.RequestBeginBlock'.header#'tendermint.abci.types.Header'.time),
                 Data),
           ABCIEvents =:= [ercoin_event:to_abci(Event) || Event <- Events]
       end).

prop_query() ->
    ?FORALL(
       {Data, Query},
       ercoin_query_gen:data_with_query(),
       begin
           {keep_state_and_data,
            {reply, "from", Response}} =
               ercoin_abci:handle_event({call, "from"}, Query, gossiping, Data),
           ercoin_query:perform(Query, Data) =:= Response
       end).
