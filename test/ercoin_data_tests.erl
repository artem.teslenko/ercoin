%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_data_tests).

-include_lib("include/ercoin.hrl").
-include_lib("triq/include/triq.hrl").

prop_shift_to_timestamp_expires_some_accounts_or_extends_their_validity() ->
    ?FORALL(
       {Data, TimestampDiff},
       {ercoin_gen:data(), non_neg_integer()},
       begin
           NewTimestamp = Data#data.timestamp + TimestampDiff,
           NewData = ercoin_data:apply_shift_to_timestamp(NewTimestamp, Data),
           ercoin_account:foldr(
             fun (_, false) ->
                     false;
                 (#account{address=Address, valid_until=ValidUntil, balance=Balance}, true) when ValidUntil < NewTimestamp ->
                     case Balance of
                         0 ->
                             none =:= ercoin_account:get(Address, NewData);
                         _ ->
                             #{fee_per_account_day := FPerAccountDay} = ercoin_fee:fees(Data),
                             #account{valid_until=NewValidUntil, balance=NewBalance} = ercoin_account:get(Address, NewData),
                             ExpectedExtensionTime =
                                 case FPerAccountDay of
                                     0 ->
                                         ?ACCOUNT_EXTENSION_DAYS;
                                     _ ->
                                         ercoin_fee:div_ceil((Balance - NewBalance) * 3600 * 24, FPerAccountDay)
                                 end,
                             NewValidUntil > ValidUntil andalso
                                 NewValidUntil - ValidUntil =:= ExpectedExtensionTime
                     end;
                 (_, true) ->
                     true
             end,
             true,
             Data#data.accounts)
       end).

prop_shift_to_timestamp_unlocks_some_accounts() ->
    ?FORALL(
       {Data, TimestampDiff},
       {ercoin_gen:data(), non_neg_integer()},
       begin
           NewTimestamp = Data#data.timestamp + TimestampDiff,
           NewData = ercoin_data:apply_shift_to_timestamp(NewTimestamp, Data),
           ercoin_account:foldr(
             fun (_, false) ->
                     false;
                 (#account{address=Address, locked_until=LockedUntil}, true) ->
                     case ercoin_account:get(Address, NewData) of
                         none ->
                             true;
                         #account{locked_until=NewLockedUntil} ->
                             case LockedUntil < NewTimestamp of
                                 true ->
                                     NewLockedUntil =:= none;
                                 false ->
                                     NewLockedUntil =:= LockedUntil
                             end
                     end
             end,
             true,
             Data#data.accounts)
       end).

prop_shift_to_timestamp_truncates_fresh_txs() ->
    ?FORALL(
       {Data, TimestampDiff},
       {ercoin_gen:data(), non_neg_integer()},
       begin
           NewTimestamp = Data#data.timestamp + TimestampDiff,
           (ercoin_data:apply_shift_to_timestamp(NewTimestamp, Data))#data.fresh_txs =:=
               ?SETS:filter(
                  fun ({TxTimestamp, _Tx}) -> TxTimestamp >= NewTimestamp - Data#data.epoch_length end,
                  Data#data.fresh_txs)
       end).

prop_shift_to_timestamp_does_not_change_money_supply() ->
    ?FORALL(
       {Data, TimestampDiff},
       {ercoin_gen:data(), non_neg_integer()},
       ercoin_data:money_supply(ercoin_data:apply_shift_to_timestamp(Data#data.timestamp + TimestampDiff, Data)) =:=
           ercoin_data:money_supply(Data)).

prop_fee_deposit_granting() ->
    ?FORALL(
       Data=
           #data{
              fee_deposit=FeeDeposit,
              validators=Validators,
              last_epoch_end=LastEpochEnd,
              timestamp=Timestamp},
       ercoin_gen:data(),
       begin
           NewData = #data{fee_deposit=NewFeeDeposit} = ercoin_data:grant_fee_deposit(Data),
           RealEpochLength = Timestamp - LastEpochEnd,
           SharesValidators = [{VP, PK} || {PK, <<VP, _/binary>>} <- gb_merkle_trees:to_orddict(Validators)],
           ValidatorsExpectedRewards = 'hare-niemeyer':apportion(SharesValidators, FeeDeposit),
           {BalancesOK, DestroyedSum} =
               lists:foldl(
                 fun ({PK, Reward}, {BalancesOKAcc, DestroyedSumAcc}) ->
                         BalanceOld = ercoin_account:lookup_balance(PK, Data),
                         BalanceNew = ercoin_account:lookup_balance(PK, NewData),
                         case ercoin_validators:absencies(PK, Validators) < RealEpochLength div 3 of
                             true ->
                                 {BalancesOKAcc andalso BalanceNew =:= BalanceOld + Reward,
                                  DestroyedSumAcc};
                             false ->
                                 {BalancesOKAcc andalso BalanceNew =:= BalanceOld,
                                  DestroyedSumAcc + Reward}
                         end
                 end,
                 {true, 0},
                 ValidatorsExpectedRewards),
           NewFeeDeposit =:= 0 andalso
               BalancesOK andalso
               ercoin_data:money_supply(NewData) =:= ercoin_data:money_supply(Data) - DestroyedSum
       end).
