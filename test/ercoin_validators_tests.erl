%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_validators_tests).

-include_lib("include/ercoin_test.hrl").

prop_drawn_validators_are_backed_by_voters() ->
    ?FORALL(
       Data=#data{last_epoch_end=LastEpochEnd, epoch_length=EpochLength},
       data(),
       begin
           Estimated2ndNextEpochStart = LastEpochEnd + 2 * EpochLength,
           lists:all(
             fun ({ValidatorAddress, _}) ->
                     ercoin_account:foldr(
                       fun
                           (_, true) ->
                               true;
                           (Account, false) ->
                               ercoin_account:get_value(validator_pk, Account) =:= ValidatorAddress andalso
                                   ercoin_account:get_value(locked_until, Account) =/= none andalso
                                   ercoin_account:get_value(locked_until, Account) > Estimated2ndNextEpochStart andalso
                                   ercoin_account:get_value(balance, Account) > 0
                       end,
                       false,
                       Data#data.accounts)
             end,
             gb_merkle_trees:to_orddict(ercoin_validators:draw(Data)))
       end).

prop_set_future_as_current_copies_votes_from_current() ->
    ?FORALL(
       Data=#data{validators=Validators},
       data(),
       ?IMPLIES(
          ercoin_epoch:stage_gte(Data, drawing_yielded),
          begin
              #data{validators=NewValidators} = ercoin_validators:set_future_as_current(Data),
              lists:all(
                fun ({Address, <<_:4/binary, MaybeVoteBin/binary>>}) ->
                        case gb_merkle_trees:lookup(Address, NewValidators) of
                            none ->
                                true;
                            <<_, 0:3/unit:8, MaybeVoteBin/binary>> ->
                                true;
                            _ ->
                                false
                        end
                end,
                gb_merkle_trees:to_orddict(Validators))
          end)).
