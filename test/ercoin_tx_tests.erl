%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_tx_tests).

-include_lib("include/ercoin_test.hrl").
-include_lib("include/ercoin_tx.hrl").

-compile({parse_transform, dynarec}).

-import(
   ercoin_tx_gen,
   [data_with_tx/0,
    data_with_tx_bin/0,
    data_with_invalid_tx_bin/0,
    data_sks_and_burn_tx/0,
    data_sks_and_transfer_tx/0,
    data_sks_and_lock_tx/0,
    data_sks_and_vote_tx/0]).

age_margin() ->
    non_neg_integer().

prop_tx_serialization() ->
    ?FORALL(Tx, ercoin_tx_gen:tx(), ercoin_tx:deserialize(ercoin_tx:serialize(Tx)) =:= Tx).

prop_valid_tx_bin_is_unpacked() ->
    ?FORALL(
       {Data, Tx},
       data_with_tx(),
       begin
           {Code, Tx} = ercoin_tx:unpack_binary(ercoin_tx:serialize(Tx), Data),
           Code =:= ?OK andalso Tx =/= none
       end).

prop_invalid_tx_bin_is_not_unpacked() ->
    ?FORALL(
       {Data, InvalidTxBin},
       data_with_invalid_tx_bin(),
       begin
           {Code, MaybeTx} = ercoin_tx:unpack_binary(InvalidTxBin, Data),
           Code =/= ?OK andalso MaybeTx =:= none
       end).

prop_handle_bin() ->
    ?FORALL(
       {Data, MaybeTxBin},
       ercoin_tx_gen:data_with_maybe_tx_bin(),
       case ercoin_tx:unpack_binary(MaybeTxBin, Data) of
           {?OK, Tx} ->
               {?OK, ercoin_tx:apply(Tx, Data)} =:=
                   ercoin_tx:handle_bin(MaybeTxBin, Data);
           {Error, none} ->
               {Error, Data} =:= ercoin_tx:handle_bin(MaybeTxBin, Data)
       end).

prop_lower_age_margin_does_not_decrease_risk_of_invalid_timestamp() ->
    ?FORALL(
       {{Data, MaybeTxBin}, AgeMargin1, AgeMargin2, Fun},
       {ercoin_tx_gen:data_with_maybe_tx_bin(),
        age_margin(),
        age_margin(),
        elements(
          [fun ercoin_tx:handle_bin/3,
           fun ercoin_tx:unpack_binary/3])},
       ?IMPLIES(
          AgeMargin1 > AgeMargin2,
          element(1, apply(Fun, [MaybeTxBin, Data, AgeMargin1])) =/= ?INVALID_TIMESTAMP orelse
          element(1, apply(Fun, [MaybeTxBin, Data, AgeMargin2])) =:= ?INVALID_TIMESTAMP)).

prop_vote_tx_changes_vote() ->
    ?FORALL(
       {{Data, _}, Tx},
       data_sks_and_vote_tx(),
       begin
           NewData = ercoin_tx:apply(Tx, Data),
           case gb_merkle_trees:lookup(ercoin_tx:from(Tx), NewData#data.validators) of
               none ->
                   true;
               _ ->
                   ercoin_vote:get(ercoin_tx:from(Tx), NewData, validators) =:= Tx#vote_tx.vote
           end andalso
               case ercoin_epoch:stage_gte(Data, drawing_yielded) of
                   true ->
                       case gb_merkle_trees:lookup(ercoin_tx:from(Tx), NewData#data.future_validators) of
                           none ->
                               true;
                           _ ->
                               ercoin_vote:get(ercoin_tx:from(Tx), NewData, future_validators) =:= Tx#vote_tx.vote
                       end;
                   false ->
                       true
               end
       end).

prop_transfer_tx_adds_balance_to_destination() ->
    ?FORALL(
       {{Data, _}, Tx=#transfer_tx{from=From, to=To, value=Value}},
       data_sks_and_transfer_tx(),
       ?IMPLIES(
          From =/= To,
          ercoin_account:lookup_balance(To, Data) + Value =:= ercoin_account:lookup_balance(To, ercoin_tx:apply(Tx, Data)))).

prop_transfer_tx_creates_account_with_minimal_validity_if_necessary() ->
    ?FORALL(
       {{Data, _}, Tx=#transfer_tx{to=To}},
       data_sks_and_transfer_tx(),
       ?IMPLIES(
          none =:= ercoin_account:get(To, Data),
          Data#data.timestamp =:= (ercoin_account:get(To, ercoin_tx:apply(Tx, Data)))#account.valid_until)).

prop_burn_tx_destroys_money() ->
    ?FORALL(
       {{Data, _}, Tx=#burn_tx{address=Address, value=Value}},
       data_sks_and_burn_tx(),
       ercoin_account:lookup_balance(Address, ercoin_tx:apply(Tx, Data)) =:=
           ercoin_account:lookup_balance(Address, Data) - Value - ercoin_fee:fee(Tx, Data)).

prop_lock_tx_locks_account_or_extends_lock_and_sets_validator_pk() ->
    ?FORALL(
       {{Data, _}, Tx=#lock_tx{address=Address, locked_until=NewLockedUntil, validator_pk=NewValidatorPK}},
       data_sks_and_lock_tx(),
       begin
           #account{locked_until=ReturnedNewLockedUntil, validator_pk=ReturnedNewValidatorPK} =
               ercoin_account:get(Address, ercoin_tx:apply(Tx, Data)),
           ReturnedNewLockedUntil =:= NewLockedUntil andalso ReturnedNewValidatorPK =:= NewValidatorPK
       end).

prop_some_txs_are_added_to_fresh_txs_and_fresh_txs_hash() ->
    ?FORALL(
       {{Data=#data{fresh_txs_hash=FreshTxsHash}, _}, Tx},
       oneof(
         [data_sks_and_transfer_tx(),
          data_sks_and_burn_tx(),
          data_sks_and_vote_tx()]),
       begin
           TxPartBin = ercoin_tx:serialize_non_malleable_part(Tx),
           #data{fresh_txs=NewFreshTxs, fresh_txs_hash=NewFreshTxsHash} = ercoin_tx:apply(Tx, Data),
           ?SETS:is_element({ercoin_tx:timestamp(Tx), ?HASH(TxPartBin)}, NewFreshTxs) andalso
               NewFreshTxsHash =:= ?HASH(<<FreshTxsHash/binary, TxPartBin/binary>>)
       end).

prop_non_burn_tx_does_not_change_money_supply() ->
    ?FORALL(
       {Data, Tx},
       data_with_tx(),
       ?IMPLIES(
          not is_record(Tx, burn_tx),
          ercoin_data:money_supply(ercoin_tx:apply(Tx, Data)) =:= ercoin_data:money_supply(Data))).

prop_tx_does_not_increase_money_supply() ->
    ?FORALL(
       {Data, Tx},
       data_with_tx(),
       ercoin_data:money_supply(ercoin_tx:apply(Tx, Data)) =< ercoin_data:money_supply(Data)).
