%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_gen).

-compile({parse_transform, dynarec}).

-include_lib("abci_server/include/abci.hrl").
-include_lib("triq/include/triq.hrl").
-include_lib("include/ercoin.hrl").

-export(
   [account/0,
    account_from/1,
    account_from/2,
    begin_block/1,
    choices/0,
    data/0,
    data_sks/0,
    data_sks_and_account/0,
    data_sks_and_account/1,
    data_with_account/0,
    data_with_begin_block/0,
    dummy_now/1,
    mutate_bin/1,
    tx_timestamp/1,
    tx_timestamp/2,
    vote/1,
    vote/2]).

-export([id/1]).

-type keypool() :: list({ercoin_sig:address(), ercoin_sig_tests:secret()}).

-spec id(any()) -> any().
%% @doc Identity.
id(Whatever) ->
    Whatever.

-spec keypool() -> triq_dom:domain(keypool()).
keypool() ->
    noshrink(
      % We make upper limit for keypool size to make tests faster.
      % We make lower limit to avoid untraced problems with ?SUCHTHAT.
      ?LET(
         Size,
         choose(4, 10),
         vector(Size, ercoin_sig_tests:address_secret()))).

-spec account(keypool(), ercoin_sig:address(), ercoin_timestamp:timestamp()) -> triq_dom:domain(account()).
account(KeyPool, Address, Timestamp) ->
    ?LET(
       {Balance,
        ValidFor,
        MaybeLockedFor,
        {MaybeValidatorPK, _}},
       {money(),
        frequency([{1, return(0)}, {1, pos_integer()}, {2, choose(1, 2 * ?MAX_EPOCH_LENGTH)}]),
        frequency([{1, return(0)}, {1, pos_integer()}, {2, choose(1, 2 * ?MAX_EPOCH_LENGTH)}, {4, return(none)}]),
        elements(KeyPool)},
       #account{
          address=Address,
          balance=Balance,
          valid_until=Timestamp + ValidFor,
          locked_until=
              case MaybeLockedFor of
                  none ->
                      none;
                  _ ->
                      Timestamp + MaybeLockedFor
              end,
          validator_pk=
              case MaybeLockedFor of
                  none ->
                      none;
                  _ ->
                      MaybeValidatorPK
              end}).

-spec account() -> triq_dom:domain(account()).
account() ->
    ?LET(
       {Address, KeyPool, Timestamp},
       {ercoin_sig_tests:address(), keypool(), timestamp()},
       account(KeyPool, Address, Timestamp)).

-spec accounts_list(keypool(), ercoin_timestamp:timestamp(), ercoin_timestamp:timestamp(), pos_integer()) -> triq_dom:domain(list(account())).
accounts_list(KeyPool, Timestamp, LastEpochEnd, EpochLength) ->
    ?SUCHTHAT(
       AccountsList,
       ?LET(
          KeyBitmap,
          vector(length(KeyPool), bool()),
          lists:foldl(
            fun ({{Address, _}, InTree}, Acc) ->
                    case InTree of
                        true ->
                            [account(KeyPool, Address, Timestamp)|Acc];
                        false ->
                            Acc
                    end
            end,
            [],
            lists:zip(KeyPool, KeyBitmap))),
       %% While the requirement below is not required by the specification most of the time, it will ease testing.
       %% Cases when there are no locked accounts suitable for voting for validators or no unlocked accounts sound pathological anyway.
       lists:any(
         fun (#account{locked_until=LockedUntil, balance=Balance}) ->
                 LockedUntil =/= none andalso LockedUntil > LastEpochEnd + 2 * EpochLength andalso Balance > 0
         end,
         AccountsList) andalso
       lists:any(
         fun (A) -> not ercoin_account:is_locked(A) end,
         AccountsList)).

-spec validators(keypool(), ercoin_timestamp:timestamp(), ercoin_timestamp:timestamp() | none) -> triq_dom:domain(gb_merkle_trees:tree()).
validators(KeyPool, Timestamp, MaybeLastEpochEnd) ->
    ?SUCHTHAT(
       Tree,
       ?LET(
          VPsVotesAbsencies,
          vector(
            length(KeyPool),
            {oneof([return(0), choose(1, 255)]),
             oneof([vote_maybe_old(Timestamp), none]),
             case MaybeLastEpochEnd of
                 none ->
                     %% We’re generating future validators, so there are no absencies yet.
                     return(0);
                 LastEpochEnd ->
                     choose(0, Timestamp - LastEpochEnd)
             end}),
          gb_merkle_trees:from_list(
            [{Address, <<VP, Absencies:3/unit:8, (ercoin_vote:serialize(Vote))/binary>>} ||
                {{Address, _}, {VP, Vote, Absencies}} <- lists:zip(KeyPool, VPsVotesAbsencies), VP > 0])),
       gb_merkle_trees:size(Tree) > 0).

-spec future_validators(keypool(), ercoin_epoch:stage(), ercoin_timestamp:timestamp()) -> triq_dom:domain(ercoin_validators:future()).
future_validators(KeyPool, Stage, Timestamp) ->
    case ercoin_epoch:stage_gte(Stage, drawing_frozen) of
       true ->
            ?LET(
               {ValidatorsTree, MaybePromiseHash},
               {validators(KeyPool, Timestamp, none), hash()},
               case ercoin_epoch:stage_gte(Stage, drawing_yielded) of
                   true ->
                       ValidatorsTree;
                   false ->
                       Key = docile_rpc:async_call(node(), ?MODULE, id, [ValidatorsTree]),
                       {promise, Key, MaybePromiseHash}
               end);
       false ->
            undefined
    end.

-spec epoch_stage() -> triq_dom:domain(ercoin_epoch:stage()).
epoch_stage() ->
    oneof(ercoin_epoch:all_stages()).

-spec dummy_now(data()) -> ercoin_timestamp:timestamp().
dummy_now(Data=#data{timestamp=Timestamp, epoch_length=EpochLength}) ->
    Timestamp + (binary:decode_unsigned(ercoin_data:app_hash(Data)) rem EpochLength).

-spec unify_votes(ercoin_validators:current(), ercoin_validators:future(), ercoin_epoch:stage()) -> ercoin_validators:current().
unify_votes(Validators, FutureValidators, Stage) ->
    case ercoin_epoch:stage_gte(Stage, drawing_yielded) of
        true ->
            gb_merkle_trees:foldr(
              fun ({Address, <<_:4/binary, FutureVoteBin/binary>>}, ValidatorsAcc) ->
                      case gb_merkle_trees:lookup(Address, ValidatorsAcc) of
                          none ->
                              ValidatorsAcc;
                          <<Header:4/binary, _/binary>> ->
                              gb_merkle_trees:enter(Address, <<Header/binary, FutureVoteBin/binary>>, ValidatorsAcc)
                      end
              end,
              Validators,
              FutureValidators);
        false ->
            Validators
    end.

data_sks() ->
    %% We use noshrink because shrinking is slow.
    noshrink(data_sks_1()).

data_sks_1() ->
    ?LET(
       {Height,
        EpochLength,
        EpochStage,
        LastEpochEnd,
        PreviousBlockTimeSinceEpoch,
        CurrentBlockLength,
        FreshTxsHash,
        FeeDeposit,
        FreshTxsStub,
        KeyPool},
       {oneof([block_height(), 0]),
        epoch_length(),
        epoch_stage(),
        timestamp(),
        non_neg_integer(),
        non_neg_integer(),
        hash(),
        money(),
        list({timestamp(), hash()}),
        keypool()},
       begin
           PreviousTimestamp = LastEpochEnd + PreviousBlockTimeSinceEpoch,
           Timestamp = PreviousTimestamp + CurrentBlockLength,
           ?LET(
              {AccountsList,
               Validators,
               FutureValidators},
              {accounts_list(KeyPool, Timestamp, LastEpochEnd, EpochLength),
               validators(KeyPool, Timestamp, LastEpochEnd),
               future_validators(KeyPool, EpochStage, Timestamp)},
              {#data{
                  height=Height,
                  last_epoch_end=LastEpochEnd,
                  epoch_stage=EpochStage,
                  previous_timestamp=PreviousTimestamp,
                  timestamp=Timestamp,
                  epoch_length=EpochLength,
                  now_fun=fun ?MODULE:dummy_now/1,
                  fee_deposit=FeeDeposit,
                  fresh_txs_hash=FreshTxsHash,
                  fresh_txs=
                      ?SETS:from_list(
                         lists:sort([{max(1, Timestamp - (Age rem EpochLength)), Hash} || {Age, Hash} <- FreshTxsStub])),
                  validators=unify_votes(Validators, FutureValidators, EpochStage),
                  future_validators=FutureValidators,
                  accounts=accounts_tree_from_list(AccountsList),
                  entropy_fun=fun ercoin_entropy:simple_entropy/1},
               maps:from_list(KeyPool)})
       end).

data() ->
    noshrink(
      ?LET(
         {Data, _},
         data_sks(),
         Data)).

hash() ->
    noshrink(binary(32)).

block_height() ->
    choose(1, ?MAX_BLOCK_HEIGHT div 2).

epoch_length() ->
    ?LET(
       I,
       pos_integer(),
       4 * I).

timestamp() ->
    non_neg_integer().

money() ->
    %% We use only 54 bits in maximum generated value to prevent money supply or fees from accidentally overflowing uint64.
    frequency([{1, return(0)}, {4, choose(1, 1 bsl 54 - 1)}]).

tx_timestamp(#data{timestamp=Timestamp, epoch_length=EpochLength}) ->
    tx_timestamp(Timestamp, EpochLength).

tx_timestamp(Timestamp, EpochLength) ->
    choose(max(Timestamp - EpochLength, 0), Timestamp + EpochLength).

choices() ->
    ?LET(
       [FeePerTx, FeePer256B, FeePerAccountDay],
       vector(3, money()),
       #{fee_per_tx => FeePerTx,
         fee_per_256_bytes => FeePer256B,
         fee_per_account_day => FeePerAccountDay,
         protocol => 1}).

vote(#data{timestamp=Timestamp, epoch_length=EpochLength}) ->
    vote(Timestamp, EpochLength).

vote(Timestamp, EpochLength) ->
    ?LET(
       {TxTimestamp, Choices},
       {tx_timestamp(Timestamp, EpochLength), choices()},
       #vote{
          timestamp=TxTimestamp,
          choices=Choices}).

vote_maybe_old(Timestamp) ->
    %% No epoch length is given, so we create a vote that possibly will not pass current validation when embedded in a tx.
    ?LET(
       {TxTimestamp, Choices},
       {choose(0, Timestamp), choices()},
       #vote{
          timestamp=TxTimestamp,
          choices=Choices}).

-spec accounts_tree_from_list(list(account())) -> gb_merkle_trees:tree().
accounts_tree_from_list(Accounts) ->
    accounts_tree_from_list(Accounts, gb_merkle_trees:empty()).

accounts_tree_from_list([], Acc) ->
    Acc;
accounts_tree_from_list([Account|Tail], Acc) ->
    {Key, Value} = ercoin_account:serialize(Account),
    NewAcc = gb_merkle_trees:enter(Key, Value, Acc),
    accounts_tree_from_list(Tail, NewAcc).

header(#data{timestamp=Timestamp}) ->
    ?LET(
       {TimestampOffset},
       {pos_integer()},
       #'tendermint.abci.types.Header'{
          time=ercoin_timestamp:to_protobuf(Timestamp + TimestampOffset)}).

begin_block(Data=#data{validators=Validators}) ->
    ?LET(
       {Header, SignedList},
       {header(Data),
        vector(gb_merkle_trees:size(Validators), bool())},
       begin
           Votes =[#'tendermint.abci.types.VoteInfo'{
                   signed_last_block=Signed,
                   validator=#'tendermint.abci.types.Validator'{address=ercoin_validators:address_to_tendermint(PK), power=Power}}
                || {{PK, <<Power, _/binary>>}, Signed} <- lists:zip(gb_merkle_trees:to_orddict(Validators), SignedList)],
           #'tendermint.abci.types.RequestBeginBlock'{
                  last_commit_info=#'tendermint.abci.types.LastCommitInfo'{votes=Votes},
                  header=Header}
       end).

data_with_begin_block() ->
    ?LET(
       Data,
       data(),
       {Data, begin_block(Data)}).

-spec filter_accounts(fun((account(), data()) -> boolean()), data()) -> list(account()).
filter_accounts(Pred, Data=#data{accounts=AccountsTree}) ->
    gb_merkle_trees:foldr(
      fun (AccountSerialized, Acc) ->
              Account = ercoin_account:deserialize(AccountSerialized),
              case Pred(Account, Data) of
                  true ->
                      [Account|Acc];
                  false ->
                      Acc
              end
      end,
      [],
      AccountsTree).

account_from(Data, Type) ->
    AccountFilter =
       case Type of
           locked ->
               fun (#account{locked_until=LockedUntil}, _) -> LockedUntil =/= none end;
           any ->
               fun (_, _) -> true end;
           unlocked ->
               fun (#account{locked_until=LockedUntil}, _) -> LockedUntil =:= none end
       end,
    AccountsList = filter_accounts(AccountFilter, Data),
    elements(AccountsList).

account_from(Data) ->
    account_from(Data, any).

data_sks_and_account(Type) ->
    ?LET(
       {Data, SKs},
       data_sks(),
       ?LET(
          Account,
          account_from(Data, Type),
          {{Data, SKs}, Account})).

data_sks_and_account() ->
    data_sks_and_account(any).

data_with_account() ->
    ?LET(
       {{Data, _}, Account},
       data_sks_and_account(),
       {Data, Account}).

-spec replace_byte(binary(), non_neg_integer(), byte()) -> binary().
replace_byte(Bin, Pos, Byte) ->
    <<Prefix:Pos/binary, _, Suffix/binary>> = Bin,
    <<Prefix/binary, Byte, Suffix/binary>>.

-spec mutate_bin(triq_dom:domain(binary())) -> triq_dom:domain(binary()).
mutate_bin(BinGen) ->
    oneof(
      %% This list is not exhaustive.
      [%% Random byte replaced.
       ?LET(
          {Bin, Byte, RandomInt},
          ?SUCHTHAT(
             {Bin, Byte, RandomInt},
             {BinGen, byte(), non_neg_integer()},
             binary:at(Bin, RandomInt rem byte_size(Bin)) =/= Byte),
          replace_byte(Bin, RandomInt rem byte_size(Bin), Byte)),
       %% Random bytes appended/prepended.
       ?LET(
          {Bin, Prefix, Suffix},
          ?SUCHTHAT(
             {_, Prefix, Suffix},
             {BinGen, binary(), binary()},
             Prefix =/= <<>> orelse Suffix =/= <<>>),
          <<Prefix/binary, Bin/binary, Suffix/binary>>)]).
