%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_tx_gen).

-include_lib("include/ercoin_test.hrl").
-include_lib("include/ercoin_tx.hrl").

-compile({parse_transform, dynarec}).

-export(
   [data_with_tx/0,
    data_with_tx_bin/0,
    data_with_invalid_tx_bin/0,
    data_with_maybe_tx_bin/0,
    data_sks_and_burn_tx/0,
    data_sks_and_transfer_tx/0,
    data_sks_and_lock_tx/0,
    data_sks_and_vote_tx/0,
    tx/0]).

-spec delete_account(ercoin_sig:address(), data()) -> data().
delete_account(Address, Data=#data{accounts=Accounts}) ->
    NewAccounts = gb_merkle_trees:delete(Address, Accounts),
    Data#data{accounts=NewAccounts}.

set_timestamp(Timestamp, Tx, SKs) ->
    sign_tx(set_timestamp_1(Timestamp, Tx), SKs).

set_timestamp_1(Timestamp, Tx=#transfer_tx{}) ->
    Tx#transfer_tx{timestamp=Timestamp};
set_timestamp_1(Timestamp, Tx=#vote_tx{}) ->
    Tx#vote_tx{vote=Tx#vote_tx.vote#vote{timestamp=Timestamp}};
set_timestamp_1(Timestamp, Tx=#burn_tx{}) ->
    Tx#burn_tx{timestamp=Timestamp}.

-spec sign_tx(tx(), ercoin_sig_tests:secret() | map()) -> tx().
sign_tx(Tx, Secrets) when is_map(Secrets) ->
    Secret = maps:get(ercoin_tx:from(Tx), Secrets),
    sign_tx(Tx, Secret);
sign_tx(Tx, Secret) ->
    ToSign = to_sign(Tx),
    Signature = ercoin_sig_tests:sign_detached(ToSign, Secret),
    set_value(signature, Signature, Tx).

-spec to_sign(tx()) -> binary().
to_sign(Tx) ->
    ercoin_tx:serialize(set_value(signature, <<>>, Tx)).

message() ->
    ?SUCHTHAT(
       Bin,
       binary(),
       byte_size(Bin) < 256).

data_sks_and_vote_tx() ->
    ?SUCHTHAT(
       {{Data, _}, Tx=#vote_tx{vote=#vote{timestamp=Timestamp}}},
       ?LET(
          {Data, SKs},
          data_sks(),
          ?LET(
             {{Address, _},
              Vote},
             {elements(
                gb_merkle_trees:to_orddict(Data#data.validators) ++
                    case ercoin_epoch:stage_gte(Data, drawing_yielded) of
                        true ->
                            gb_merkle_trees:to_orddict(Data#data.future_validators);
                        false ->
                            []
                    end),
              vote(Data)},
             {{Data, SKs},
              sign_tx(
                #vote_tx{
                   address=Address,
                   vote=Vote,
                   signature= <<0:512>>},
                SKs)})),
       case ercoin_vote:lookup(ercoin_tx:from(Tx), Data) of
           none ->
               true;
           #vote{timestamp=CurrentTimestamp} ->
               CurrentTimestamp < Timestamp
       end).

data_sks_and_lock_tx() ->
    ?LET(
       {{{Data=#data{timestamp=Timestamp}, SKs}, #account{address=Address, locked_until=LockedUntil}},
        LockedFor,
        ValidatorPK},
       {data_sks_and_account(),
        pos_integer(),
        ercoin_sig_tests:address()},
       begin
           NewLockedUntil =
               case LockedUntil of
                   none ->
                       Timestamp + LockedFor;
                   _ ->
                       max(Timestamp, LockedUntil) + LockedFor
               end,
           Tx =
               sign_tx(
                 #lock_tx{
                    address=Address,
                    locked_until=NewLockedUntil,
                    validator_pk=ValidatorPK,
                    signature= <<0:512>>},
                 SKs),
           {{make_sufficient_balance(Data, Tx), SKs}, Tx}
       end).

-spec make_sufficient_balance(data(), tx()) -> data().
make_sufficient_balance(Data, Tx) ->
    Account = ercoin_account:get(ercoin_tx:from(Tx), Data),
    NewAccount = Account#account{balance=max(ercoin_tx:required_balance(Tx, Data), Account#account.balance)},
    ercoin_account:put(NewAccount, Data).

transfer_tx({Data, SKs}, From, ToAddress) ->
    ?LET(
       {Value, Timestamp, Message},
       {choose(0, From#account.balance), ercoin_gen:tx_timestamp(Data), message()},
       sign_tx(
         #transfer_tx{
            timestamp=Timestamp,
            from=From#account.address,
            to=ToAddress,
            message=Message,
            value=Value,
            signature= <<0:512>>},
         SKs
        )).

burn_tx({Data, SKs}, #account{address=Address, balance=Balance}) ->
    ?LET(
       {Value, Timestamp, Message},
       {choose(0, Balance), ercoin_gen:tx_timestamp(Data), message()},
       sign_tx(
         #burn_tx{
            timestamp=Timestamp,
            address=Address,
            message=Message,
            value=Value,
            signature= <<0:512>>},
         SKs)).

data_sks_and_transfer_tx() ->
    ?LET(
       {Data, SKs},
       data_sks(),
       ?LET(
          {From, ToAddress},
          {ercoin_gen:account_from(Data, unlocked), oneof(maps:keys(SKs))},
          ?LET(
             Tx,
             transfer_tx({Data, SKs}, From, ToAddress),
             {{make_sufficient_balance(Data, Tx), SKs}, Tx}))).

data_sks_and_burn_tx() ->
    ?LET(
       {Data, SKs},
       data_sks(),
       ?LET(
          From,
          ercoin_gen:account_from(Data, unlocked),
          ?LET(
             Tx,
             burn_tx({Data, SKs}, From),
             {{make_sufficient_balance(Data, Tx), SKs}, Tx}))).

-spec data_with_invalid_tx_bin() -> triq_dom:domain({data(), binary()}).
data_with_invalid_tx_bin() ->
    %% Tx is returned as binary.
    oneof(
      [%% Random binary.
       ?LET(
          {Data, RandomBin},
          {data(), binary()},
          {Data, RandomBin}),
       %% Valid binary randomly mutated.
       ?LET(
          {Data, TxBin},
          data_with_tx_bin(),
          {Data, ercoin_gen:mutate_bin(TxBin)}),
       %% Insufficient balance.
       ?LET(
          {{{Data, _}, Tx},
           LackingFunds},
          {?SUCHTHAT(
              {{Data, _}, Tx},
              data_sks_and_tx(),
              not is_record(Tx, vote_tx) andalso ercoin_fee:fee(Tx, Data) > 0),
           pos_integer()},
          begin
              Account = ercoin_account:get(ercoin_tx:from(Tx), Data),
              InvalidAccount = Account#account{balance=max(0, ercoin_tx:required_balance(Tx, Data) - LackingFunds)},
              {ercoin_account:put(InvalidAccount, Data),
               ercoin_tx:serialize(Tx)}
          end),
       %% Bad signature.
       ?LET(
          {{Data, Secrets}, Tx},
          data_sks_and_tx(),
          begin
              Secret = maps:get(ercoin_tx:from(Tx), Secrets),
              ToSign = to_sign(Tx),
              {Data, ercoin_sig_tests:invalid_sign(ToSign, Secret)}
          end),
       %% Lock not longer than existing.
       ?LET(
          {{{Data, _}, Tx}, ShorterFor, ValidatorPK},
          {data_sks_and_lock_tx(), non_neg_integer(), ercoin_sig_tests:address()},
          begin
              Account = ercoin_account:get(ercoin_tx:from(Tx), Data),
              {ercoin_account:put(
                 Account#account{
                   locked_until=Tx#lock_tx.locked_until + ShorterFor,
                   validator_pk=ValidatorPK},
                 Data),
               ercoin_tx:serialize(Tx)}
          end),
       %% Locked until shorter than timestamp.
       ?LET(
          {{{Data=#data{timestamp=Timestamp}, SKs}, Tx}, ShorterFor},
          {data_sks_and_lock_tx(), pos_integer()},
          {Data, ercoin_tx:serialize(sign_tx(Tx#lock_tx{locked_until=max(0, Timestamp - ShorterFor)}, SKs))}),
       %% From not existing.
       ?LET(
          {Data, ValidTx},
          ?SUCHTHAT(
             {_, Tx},
             data_with_tx(),
             not is_record(Tx, vote_tx)),
          {delete_account(ercoin_tx:from(ValidTx), Data), ercoin_tx:serialize(ValidTx)}),
       %% Vote tx: From not a validator and not a future validator.
       ?LET(
          {{Data=#data{validators=Validators, future_validators=FutureValidators}, _}, ValidTx=#vote_tx{address=Address}},
          ?SUCHTHAT(
             {{#data{validators=Validators, future_validators=FutureValidators, epoch_stage=Stage}, _}, _},
             data_sks_and_vote_tx(),
             gb_merkle_trees:size(Validators) > 1 andalso (not ercoin_epoch:stage_gte(Stage, drawing_yielded) orelse gb_merkle_trees:size(FutureValidators) > 1)),
          begin
              NewValidators =
                  case gb_merkle_trees:lookup(Address, Validators) of
                      none ->
                          Validators;
                      _ ->
                          gb_merkle_trees:delete(Address, Validators)
                  end,
              NewFutureValidators =
                  case ercoin_epoch:stage_gte(Data, drawing_yielded) of
                      true ->
                          case gb_merkle_trees:lookup(Address, FutureValidators) of
                              none ->
                                  FutureValidators;
                              _ ->
                                  gb_merkle_trees:delete(Address, FutureValidators)
                          end;
                      false ->
                          FutureValidators
                  end,
              {Data#data{validators=NewValidators, future_validators=NewFutureValidators},
               ercoin_tx:serialize(ValidTx)}
          end),
       %% Transfer/burn tx: from locked.
       ?LET(
          {{{Data, _}, Tx},
           LockedFor,
           ValidatorPK},
          {oneof(
             [fun data_sks_and_transfer_tx/0,
              fun data_sks_and_burn_tx/0]),
           non_neg_integer(),
           ercoin_sig_tests:address()},
          begin
              Account = ercoin_account:get(ercoin_tx:from(Tx), Data),
              NewData =
                  ercoin_account:put(
                    Account#account{
                      locked_until=Data#data.timestamp + LockedFor,
                      validator_pk=ValidatorPK},
                    Data),
              {NewData, ercoin_tx:serialize(Tx)}
          end),
       %% Tx not valid yet.
       ?LET(
          {{Data, SKs}, Tx},
          oneof(
            [fun data_sks_and_transfer_tx/0,
             fun data_sks_and_vote_tx/0,
             fun data_sks_and_burn_tx/0]),
          ?LET(
             ValidIn,
             pos_integer(),
             {Data, ercoin_tx:serialize(set_timestamp(Data#data.timestamp + Data#data.epoch_length + ValidIn, Tx, SKs))})),
       %% Tx outdated.
       ?LET(
          {{Data=#data{timestamp=Timestamp, epoch_length=EpochLength}, SKs}, Tx},
          ?SUCHTHAT(
             {{#data{timestamp=Timestamp, epoch_length=EpochLength}, _}, _},
             oneof(
               [fun data_sks_and_transfer_tx/0,
                fun data_sks_and_vote_tx/0,
                fun data_sks_and_burn_tx/0]),
             Timestamp > EpochLength),
          ?LET(
             ExpiredBy,
             choose(1, Timestamp - EpochLength),
             {Data, ercoin_tx:serialize(set_timestamp(Timestamp - EpochLength - ExpiredBy, Tx, SKs))})),
       %% Newer vote already present.
       ?LET(
          {{Data, _}, Tx},
          data_sks_and_vote_tx(),
          ?LET(
             {NewerVoteStub, NewerTimestamp},
             {vote(Data),
              choose(
                Tx#vote_tx.vote#vote.timestamp,
                Data#data.timestamp + ercoin_tx:default_age_margin(Data))},
             {ercoin_vote:put(
                ercoin_tx:from(Tx),
                NewerVoteStub#vote{timestamp=NewerTimestamp},
                Data),
              ercoin_tx:serialize(Tx)})),
       %% Incorrectly specified message length.
       ?LET(
          {{{Data, SKs}, ValidTx}, InvalidLength},
          ?SUCHTHAT(
             {{_, ValidTx},
              InvalidLength},
             {oneof(
                [fun data_sks_and_transfer_tx/0,
                 fun data_sks_and_burn_tx/0]),
              choose(0, 255)},
             InvalidLength =/= byte_size(get_value(message, ValidTx))),
          begin
              Message = get_value(message, ValidTx),
              ToSign = to_sign(ValidTx),
              TxHead = binary:part(ToSign, 0, byte_size(ToSign) - byte_size(Message) - 1),
              NewToSign = <<TxHead/binary, InvalidLength, Message/binary>>,
              {Data, ercoin_sig_tests:sign(NewToSign, maps:get(ercoin_tx:from(ValidTx), SKs))}
          end),
       %% Replayed tx.
       ?LET(
          {{Data, _}, Tx},
          oneof(
            [fun data_sks_and_transfer_tx/0,
             fun data_sks_and_vote_tx/0,
             fun data_sks_and_burn_tx/0]),
          begin
              TxBin = ercoin_tx:serialize(Tx),
              TxPartBin = ercoin_tx:serialize_non_malleable_part(Tx),
              #data{fresh_txs=FreshTxs} = Data,
              NewFreshTxs = ?SETS:add_element({ercoin_tx:timestamp(Tx), ?HASH(TxPartBin)}, FreshTxs),
              {Data#data{fresh_txs=NewFreshTxs}, TxBin}
          end)
       %% TODO: Maybe ensure that no more than one vote tx per validator can be included in one block.
      ]).

data_sks_and_tx() ->
    noshrink(
      oneof(
        [fun data_sks_and_transfer_tx/0,
         fun data_sks_and_lock_tx/0,
         fun data_sks_and_vote_tx/0,
         fun data_sks_and_burn_tx/0])).

data_with_tx() ->
    noshrink(
      ?LET(
         {{Data, _}, Tx},
         data_sks_and_tx(),
         {Data, Tx})).

data_with_tx_bin() ->
    ?LET(
       {{Data, _}, Tx},
       data_sks_and_tx(),
       {Data, ercoin_tx:serialize(Tx)}).

data_with_maybe_tx_bin() ->
    oneof(
      [fun data_with_tx_bin/0,
       fun data_with_invalid_tx_bin/0]).

tx() ->
    noshrink(
      ?LET(
         {_, Tx},
         data_sks_and_tx(),
         Tx)).
