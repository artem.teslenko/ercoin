-include_lib("triq/include/triq.hrl").
-include_lib("include/ercoin.hrl").

-import(
   ercoin_gen,
   [account/0,
    begin_block/1,
    data/0,
    data_sks/0,
    data_with_account/0,
    data_with_begin_block/0,
    data_sks_and_account/0,
    data_sks_and_account/1,
    vote/1,
    vote/2]).
