%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-type message() :: binary().
-record(
   transfer_tx,
   {timestamp :: ercoin_timestamp:timestamp(),
    from :: ercoin_sig:address(),
    to :: ercoin_sig:address(),
    value :: money(),
    message :: message(),
    signature :: ercoin_sig:sig()}).
-type transfer_tx() :: #transfer_tx{}.
-record(
   burn_tx,
   {timestamp :: ercoin_timestamp:timestamp(),
    address :: ercoin_sig:address(),
    value :: money(),
    message :: message(),
    signature :: ercoin_sig:sig()}).
-type burn_tx() :: #burn_tx{}.
-record(
   lock_tx,
   {address :: ercoin_sig:address(),
    locked_until :: ercoin_timestamp:timestamp(),
    validator_pk :: ercoin_sig:address(),
    signature :: ercoin_sig:sig()}).
-type lock_tx() :: #lock_tx{}.
-record(
   vote_tx,
   {vote :: vote(),
    address :: ercoin_sig:address(),
    signature :: ercoin_sig:sig()}).
-type vote_tx() :: #vote_tx{}.
-type tx() :: transfer_tx() | lock_tx() | vote_tx() | burn_tx().
