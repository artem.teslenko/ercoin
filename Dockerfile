FROM erlang:21-alpine

WORKDIR /app

COPY . /app

RUN apk add --no-cache make git curl build-base libtool autoconf automake libsodium-dev

RUN make rel

EXPOSE 26658

CMD ["./_rel/ercoin_release/bin/ercoin_release", "foreground"]
