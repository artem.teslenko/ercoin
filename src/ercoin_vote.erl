%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_vote).

-compile({parse_transform, dynarec}).

-export(
   [choices_serialize/1,
    deserialize/1,
    get/3,
    lookup/2,
    put/3,
    serialize/1]).

-include_lib("include/ercoin.hrl").

-spec choices_serialize(choices()) -> binary().
choices_serialize(Choices=#{protocol := Protocol}) ->
    <<(ercoin_fee:serialize(Choices))/binary, Protocol>>.

-spec serialize(vote() | none) -> binary().
serialize(#vote{timestamp=Timestamp, choices=Choices}) ->
    <<Timestamp:4/unit:8, (choices_serialize(Choices))/binary>>;
serialize(none) ->
    <<>>.

-spec deserialize(binary()) -> vote() | none.
deserialize(<<Timestamp:4/unit:8, FeesBin:24/binary, Protocol>>) ->
    #vote{
       timestamp=Timestamp,
       choices=maps:put(protocol, Protocol, ercoin_fee:deserialize(FeesBin))};
deserialize(<<>>) ->
    none.

-spec put(ercoin_sig:address(), vote(), data()) -> data().
put(Address, Vote, Data=#data{validators=Validators, future_validators=FutureValidators}) ->
    VoteBin = ercoin_vote:serialize(Vote),
    NewValidators =
        case gb_merkle_trees:lookup(Address, Validators) of
            <<Power, AbsenciesBin:3/binary, _/binary>> ->
                gb_merkle_trees:enter(Address, <<Power, AbsenciesBin:3/binary, VoteBin/binary>>, Validators);
            none ->
                Validators
        end,
    NewFutureValidators =
        case ercoin_epoch:stage_gte(Data, drawing_yielded) of
            true ->
                case gb_merkle_trees:lookup(Address, FutureValidators) of
                    <<Power2, 0:3/unit:8, _/binary>> ->
                        gb_merkle_trees:enter(Address, <<Power2, 0:3/unit:8, VoteBin/binary>>, FutureValidators);
                    none ->
                        FutureValidators
                end;
            false ->
                FutureValidators
        end,
    Data#data{validators=NewValidators, future_validators=NewFutureValidators}.

-spec get(ercoin_sig:address(), data(), atom()) -> vote() | none.
get(Address, Data, Field) ->
    <<_:1/unit:8, _:3/unit:8, VoteBin/binary>> = gb_merkle_trees:lookup(Address, get_value(Field, Data)),
    ercoin_vote:deserialize(VoteBin).

-spec lookup(ercoin_sig:address(), data()) -> vote() | none | validator_not_found.
lookup(Address, Data) ->
    case gb_merkle_trees:lookup(Address, Data#data.validators) of
        <<_:4/binary, VoteBin/binary>> ->
            deserialize(VoteBin);
        none ->
            case ercoin_epoch:stage_gte(Data, drawing_yielded) of
                true ->
                    case gb_merkle_trees:lookup(Address, Data#data.future_validators) of
                        <<_:4/binary, VoteBin2/binary>> ->
                            deserialize(VoteBin2);
                        none ->
                            validator_not_found
                    end;
                false ->
                    validator_not_found
            end
    end.
