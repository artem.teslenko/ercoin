%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_validators).

-include_lib("include/ercoin.hrl").

-export(
   [absencies/2,
    address_to_tendermint/1,
    begin_drawing/1,
    current_hash/1,
    draw/1,
    future_hash/1,
    key_value_by_tendermint_address/2,
    set_future_as_current/1,
    yield_drawing/1]).

-export_type(
   [current/0,
    future/0]).

-type current() :: gb_merkle_trees:tree().
-type future() :: gb_merkle_trees:tree() | {promise, docile_rpc:key(), hash()} | undefined.

-spec voting_resolution(data()) -> pos_integer().
voting_resolution(_) ->
    20.

-spec entropy(data()) -> binary().
entropy(Data=#data{entropy_fun=F}) ->
    apply(F, [Data]).

-spec draw(data()) -> gb_merkle_trees:tree().
draw(Data=#data{accounts=Accounts, timestamp=Timestamp, epoch_length=EpochLength, last_epoch_end=LastEpochEnd}) ->
    Entropy = entropy(Data),
    Resolution = voting_resolution(Data),
    ThreeYears = 3 * 365 * 24 * 3600,
    Estimated2ndNextEpochStart = LastEpochEnd + 2 * EpochLength,
    {PointsAddresses, TotalPoints} =
        gb_merkle_trees:foldr(
          fun (AccountSerialized, {PointsAddresses1, PointsSum}) ->
                  #account{locked_until=LockedUntil, validator_pk=ValidatorPK, balance=Balance} = ercoin_account:deserialize(AccountSerialized),
                  case LockedUntil =/= none andalso LockedUntil >= Timestamp of
                      false ->
                          {PointsAddresses1, PointsSum};
                      true ->
                          RemainingLockPeriod = max(0, LockedUntil - Estimated2ndNextEpochStart),
                          Points = min(RemainingLockPeriod, ThreeYears) * Balance,
                          {[{Points, ValidatorPK}|PointsAddresses1], PointsSum + Points}
                  end
          end,
          {[], 0},
          Accounts),
    RandState = <<"apportionment", Entropy/binary>>,
    gb_merkle_trees:from_orddict(
      [{Address, case gb_merkle_trees:lookup(Address, Data#data.validators) of
                     none ->
                         <<VotePower, 0:3/unit:8>>;
                     <<_, _:3/unit:8, VoteBin2/binary>> ->
                         <<VotePower, 0:3/unit:8, VoteBin2/binary>>
                 end} ||
          {Address, VotePower} <- random_apportionment:apportion(PointsAddresses, Resolution, TotalPoints, RandState)]).

-spec absencies(ercoin_sig:address(), gb_merkle_trees:tree()) -> non_neg_integer().
absencies(Address, Validators) ->
    <<_, Absencies:3/unit:8, _/binary>> = gb_merkle_trees:lookup(Address, Validators),
    Absencies.

-spec address_to_tendermint(ercoin_sig:address()) -> binary().
%% @doc Convert validator’s public key to Tendermint address.
address_to_tendermint(Address) ->
    <<TendermintAddress:20/binary, _/binary>> = crypto:hash(sha256, Address),
    TendermintAddress.

-spec tendermint_address_map(gb_merkle_trees:tree()) -> #{binary() => ercoin_sig:address()}.
%% @doc Return a map of Tendermint addresses to Ercoin addresses (validators’ public keys).
tendermint_address_map(Validators) ->
    maps:from_list([{address_to_tendermint(Address), Address} || {Address, _} <- gb_merkle_trees:to_orddict(Validators)]).

-spec key_value_by_tendermint_address(binary(), data()) -> {ercoin_sig:address(), binary()}.
key_value_by_tendermint_address(TAddress, #data{validators=Validators}) ->
    PK = maps:get(TAddress, tendermint_address_map(Validators)),
    {PK, gb_merkle_trees:lookup(PK, Validators)}.

-spec set_future_as_current(data()) -> data().
set_future_as_current(Data=#data{validators=Validators, future_validators=FutureValidators}) ->
    %% We copy votes because there may exist votes put by current validators when drawing was frozen.
    NewValidators =
        gb_merkle_trees:foldr(
          fun ({Address, <<_:4/binary, MaybeVoteBin/binary>>}, NewValidatorsAcc) ->
                  case gb_merkle_trees:lookup(Address, NewValidatorsAcc) of
                      none ->
                          NewValidatorsAcc;
                      <<VP, 0:3/unit:8, _/binary>> ->
                          gb_merkle_trees:enter(
                            Address,
                            <<VP, 0:3/unit:8, MaybeVoteBin/binary>>,
                            NewValidatorsAcc)
                  end
          end,
          FutureValidators,
          Validators),
    Data#data{validators=NewValidators, future_validators=undefined}.

-spec begin_drawing(data()) -> data().
begin_drawing(Data) ->
    Key = docile_rpc:async_call(node(), ercoin_validators, draw, [Data]),
    Data#data{future_validators={promise, Key, ercoin_data:app_hash(Data)}}.

-spec yield_drawing(data()) -> data().
yield_drawing(Data=#data{future_validators={promise, Key, _}}) ->
    %% During normal operation, the result should be already available (some timeout may be needed anyway to deliver it),
    %% but when we’re syncing, we may need to wait a bit.
    {value, NewFutureValidators} = docile_rpc:nb_yield(Key, 7000),
    Data#data{future_validators=NewFutureValidators}.

-spec current_hash(current()) -> hash().
current_hash(Validators) ->
    gb_merkle_trees:root_hash(Validators).

-spec future_hash(future()) -> hash().
future_hash(undefined) ->
    <<0:256>>;
future_hash({promise, _, Hash}) ->
    Hash;
future_hash(Validators) ->
    gb_merkle_trees:root_hash(Validators).
