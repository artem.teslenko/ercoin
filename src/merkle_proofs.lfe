;; Licensed under the Apache License, Version 2.0 (the “License”);
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;     http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an “AS IS” BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(defmodule merkle_proofs
  (export (deserialize-path 1)
          (serialize-path 1)
          (fold 1)
          )
  (export_type (path 0)
               ))

(deftype path (list (tuple (UNION 'left 'right) (binary))))

(defspec (deserialize-path 1)
  (((binary)) (UNION (path) 'none)))
(defun deserialize-path
  (((binary))
   (list))
  (((binary (direction-int) (step binary (size 32)) (rest-binary binary)))
   (case (deserialize-path rest-binary)
     ('none 'none)
     (rest (let ((direction (case direction-int
                              (1 'right)
                              (0 'left)
                              (_ 'none))))
             (case direction
               ('none 'none)
               (_ (cons
                   (tuple direction step) rest)))))))
  ((_)
   'none))

(defspec (serialize-path 1) (((path)) (binary)))
(defun serialize-path
  (((list))
   (binary))
  (((cons (tuple direction hash) steps))
   (let ((steps-bin (serialize-path steps))
         (direction-int (case direction
                          ('left 0)
                          ('right 1))))
     (binary (direction-int) (hash binary) (steps-bin binary)))))

(defspec (fold 1)
  (((tuple (binary) (path))) (binary)))
(defun fold
  "Compute root hash from a Merkle proof."
  (((tuple val (list)))
   val)
  (((tuple val (cons (tuple direction step-val) steps)))
   (let ((to-hash (case direction
                    ('left (binary (step-val binary) (val binary)))
                    ('right (binary (val binary) (step-val binary))))))
     (fold (tuple (crypto:hash 'sha256 to-hash) steps)))))
