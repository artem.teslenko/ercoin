%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

%% @reference <a href="https://hackage.haskell.org/package/burnt-explorer-2.0.0">burnt-explorer 2.0.0</a> — it is assumed that it is used to generate files containing burnt BlackCoin outputs.
-module(ercoin_genesis).

-compile({parse_transform, category}).

-export(
   [binary_to_hex/1,
    data_to_genesis_json/2,
    initial_distribution/2,
    initial_data/1,
    filter_genesis_txs/0,
    line_to_score/1,
    payload_from_script_hex/1,
    sk_to_priv_validator_key_json/1,
    testnet_data/0,
    testnet_data/1]).

-import(
   datum_cat_option,
   [fail/1,
    unit/1]).

-include_lib("include/ercoin.hrl").
-include_lib("include/ercoin_tx.hrl").

%% Bitcoin Script, some opcodes.
-define(OP_RETURN, 106).
-define(OP_PUSHDATA1, 76).
-define(OP_PUSHDATA2, 77).
-define(OP_PUSHDATA4, 78).

-define(SATOSHIS_IN_BLK, 100000000).
-define(BLK_DUST_THRESHOLD, 3 * ?SATOSHIS_IN_BLK). %% In satoshis.
-define(MAX_LOCKED_FOR, 110376000).
-define(INITIAL_VALID_FOR, 3600 * 24 * 3).

-define(DISTRIBUTION_START, 1573171200).
-define(DISTRIBUTION_END, 1575763199).
-define(LATE_TOLERANCE, 3600 * 12).  %% The precise value doesn’t have much importance, as there is only one late transaction under consideration.
-define(TOTAL_SUPPLY, 2600000000000000).
-define(GENESIS_TIME, 1579737600).
%% Score extracted from burnt output.
-type score() :: {SatoshisBurnt :: non_neg_integer(), {ercoin_sig:address(), LockedFor :: non_neg_integer() | none, ValidatorAddress :: ercoin_sig:address() | none}}.

hex(N) when N < 10 ->
    $0+N;
hex(N) when N >= 10, N < 16 ->
    $a+(N-10).

to_hex(N) when N < 256 ->
    [hex(N div 16), hex(N rem 16)].

-spec binary_to_iolist(binary()) -> iolist().
binary_to_iolist(<<>>) ->
    [];
binary_to_iolist(<<Head/integer, Rest/binary>>) ->
    to_hex(Head) ++ binary_to_iolist(Rest).

-spec binary_to_hex(binary()) -> binary().
binary_to_hex(Bin) ->
    iolist_to_binary(binary_to_iolist(Bin)).

-spec hexstr_to_bin(string()) -> binary().
hexstr_to_bin(String) ->
    hexstr_to_bin(String, <<>>).

-spec hexstr_to_bin(string(), binary()) -> binary().
hexstr_to_bin("", Acc) ->
    Acc;
hexstr_to_bin([X,Y|Tail], Acc) ->
    {ok, [Byte], []} = io_lib:fread("~16u", [X,Y]),
    hexstr_to_bin(Tail, <<Acc/binary, Byte>>).

%% @doc Convert data to a genesis.json file for Tendermint.
-spec data_to_genesis_json(data(), binary()) -> binary().
data_to_genesis_json(Genesis, ChainId) ->
    Options = [pretty],
    Validators =
        [{[{<<"power">>, integer_to_binary(Power)},
           {<<"name">>, <<"">>},
           {<<"pub_key">>,
            {[{<<"type">>, <<"tendermint/PubKeyEd25519">>},
              {<<"value">>, base64:encode(PK)}]}}]}
         || {PK, <<Power/integer, _/binary>>} <- gb_merkle_trees:to_orddict(Genesis#data.validators)],
    ToEncode =
        {[{<<"app_hash">>, <<"">>},
          {<<"app_state">>, base64:encode(term_to_binary(Genesis, [{compressed, 9}]))},
          {<<"genesis_time">>, ercoin_timestamp:to_iso(Genesis#data.last_epoch_end)},
          {<<"chain_id">>, ChainId},
          {<<"validators">>, Validators}]},
    jiffy:encode(ToEncode, Options).

sk_to_priv_validator_key_json(SK) ->
    Options = [pretty],
    <<_:32/binary, PK:32/binary>> = SK,
    Address = binary:part(crypto:hash(sha256, PK), 0, 20),
    ToEncode =
        {[{<<"address">>, binary_to_hex(Address)},
          {<<"priv_key">>,
           {[{<<"type">>, <<"tendermint/PrivKeyEd25519">>},
            {<<"value">>, base64:encode(SK)}]}}]},
    jiffy:encode(ToEncode, Options).

%% Functions for generating initial data below.

-spec payload_from_script_hex(string()) -> datum:option(binary()).
payload_from_script_hex(ScriptHex) ->
    case hexstr_to_bin(ScriptHex) of
        <<?OP_RETURN, Length, Payload2:Length/binary>> when Length >= 1, Length =< 75 ->
            unit(Payload2);
        <<?OP_RETURN, ?OP_PUSHDATA1, Length, Payload2:Length/binary>> ->
            unit(Payload2);
        <<?OP_RETURN, ?OP_PUSHDATA2, Length:2/unit:8, Payload2:Length/binary>> ->
            unit(Payload2);
        <<?OP_RETURN, ?OP_PUSHDATA4, Length:4/unit:8, Payload2:Length/binary>> ->
            unit(Payload2);
        _ ->
            fail("Script not recognized.")
    end.

-spec payload_amount_to_score(binary(), non_neg_integer(), BeLiberal :: boolean()) -> datum:option(score()).
payload_amount_to_score(
  <<"Ercoin "/utf8,
    0,
    1,
    Address:32/binary,
    Rest/binary>>,
  Amount,
  true) when byte_size(Rest) < 64 ->
    {Amount, {Address, none, none}};
payload_amount_to_score(
  <<"Ercoin "/utf8,
    LockedForLength,
    LockedFor:LockedForLength/unit:8,
    Rest/binary>>=Payload,
  Amount,
  _)
  when LockedForLength =< 4 ->
    MsgLength = 7 + 1 + LockedForLength +
        case LockedFor of
            0 -> 0;
            _ -> 32
        end,
    <<Msg:MsgLength/binary, Sig/binary>> = Payload,
    [option ||
        Address <- ercoin_sig:verify_and_unpack_address(Sig, Msg),
        (fun () ->
                 case LockedFor > 0 of
                     true ->
                         case Rest of
                             <<ValidatorAddress:32/binary, _/binary>> ->
                                 unit(
                                   {Amount,
                                    {Address, LockedFor, ValidatorAddress}});
                             _ ->
                                 fail("Validator address not present.")
                         end;
                     false ->
                         unit(
                           {Amount, {Address, none, none}})
                 end
         end)()];
payload_amount_to_score(_, _, _) ->
    fail("Unrecognized format.").

-spec distribution_timestamps_ok(ercoin_timestamp:timestamp(), string(), boolean()) -> boolean().
distribution_timestamps_ok(BlockTimestamp, TxId, BeLiberal) ->
    BlockTimestamp >= ?DISTRIBUTION_START
        andalso
          (BlockTimestamp =< ?DISTRIBUTION_END
           orelse BeLiberal andalso BlockTimestamp =< ?DISTRIBUTION_END + ?LATE_TOLERANCE andalso distribution_tx_timestamp_ok(BlockTimestamp, TxId)).

-spec distribution_tx_timestamp_ok(ercoin_tx:timestamp(), string()) -> boolean().
%% @doc Tell whether transaction timestamp fits into the distribution period.
%%
%% burnt-explorer does not provide transaction timestamps, so we just hardcode the results.
distribution_tx_timestamp_ok(_, "4aedfffc7bb90589cef0f8919165169824efa2caeb99f842650e56bff1218a76") ->
    true;
distribution_tx_timestamp_ok(BlockTimestamp, _) when BlockTimestamp >= ?DISTRIBUTION_START andalso BlockTimestamp =< ?DISTRIBUTION_END + ?LATE_TOLERANCE ->
    false.

-spec deduct_missing_fees(score(), string()) -> score().
deduct_missing_fees({Amount, RestScore}, TxId) ->
    ToDeduct =
        % We just hardcode all cases from the relevant time period.
        case TxId of
            % Fee was not sufficient to be relayed/confirmed by the BlackCoin Original wallet. See https://gitlab.com/Ercoin/ercoin/issues/8 for details.
            "4aedfffc7bb90589cef0f8919165169824efa2caeb99f842650e56bff1218a76" ->
                9820;
            _ ->
                0
        end,
    {Amount - ToDeduct, RestScore}.

-spec line_to_score(string()) -> datum:option(score()).
line_to_score(Line) ->
    line_to_score(Line, true).

-spec line_to_score(string(), boolean()) -> datum:option(score()).
line_to_score(Line, BeLiberal) ->
    [_, _, BlockTimestampStr, TxId, _, AmountStr, ScriptHex|_] = string:split(Line, ",", all),
    [option ||
        Payload <- payload_from_script_hex(ScriptHex),
        cats:require(
          distribution_timestamps_ok(list_to_integer(BlockTimestampStr), TxId, BeLiberal),
          payload_amount_to_score(Payload, list_to_integer(AmountStr), BeLiberal),
          "Invalid timestamps."),
        deduct_missing_fees(_, TxId)].

-spec filter_lines(fun((string()) -> datum:option(any()))) -> ok.
%% @doc Filter lines from stdin which mean something to the provided function. Output to stdout.
filter_lines(F) ->
    case io:get_line("") of
        eof ->
            ok;
        Line ->
            ok =
                case F(string:trim(Line)) of
                    undefined ->
                        ok;
                    _ ->
                        io:format("~s", [Line])
                end,
            ?FUNCTION_NAME(F)
    end.

-spec filter_genesis_txs() -> ok.
filter_genesis_txs() ->
    filter_lines(fun line_to_score/1).

-spec extract_from_file_lines(file:io_device(), fun((string()) -> datum:option(T))) -> list(T).
%% @doc Extract data from file using a specified function. Returns a list in reversed order.
extract_from_file_lines(File, F) ->
    extract_from_file_lines(File, F, []).

-spec extract_from_file_lines(file:io_device(), fun((string()) -> {ok, T} | none), list(T)) -> list(T).
extract_from_file_lines(File, F, Acc) ->
    case file:read_line(File) of
        eof ->
            Acc;
        {ok, Line} ->
            case F(Line) of
                undefined ->
                    ?FUNCTION_NAME(File, F, Acc);
                Value ->
                    ?FUNCTION_NAME(File, F, [Value|Acc])
            end
    end.

-spec scores_to_distribution(list(score())) -> gb_merkle_trees:tree().
scores_to_distribution(Scores) ->
    ValidFor = ?INITIAL_VALID_FOR,
    Distribution = 'hare-niemeyer':apportion(Scores, ?TOTAL_SUPPLY),
    AccountsList =
        lists:foldl(
          fun ({{Address, LockedFor, ValidatorPK}, Balance}, Acc) ->
                  Account =
                      #account{
                         address=Address,
                         validator_pk=ValidatorPK,
                         balance=Balance,
                         valid_until=?GENESIS_TIME + ValidFor,
                         locked_until=
                             case LockedFor of
                                 none ->
                                     none;
                                 _ ->
                                     ?GENESIS_TIME + min(LockedFor, ?MAX_LOCKED_FOR)
                             end},
                  [ercoin_account:serialize(Account)|Acc]
          end,
          [],
          Distribution),
    gb_merkle_trees:from_orddict(lists:sort(AccountsList)).

-spec max_on_maybe(none | integer(), none | integer()) -> none | integer().
max_on_maybe(none, Sth) ->
    Sth;
max_on_maybe(Sth, none) ->
    Sth;
max_on_maybe(Int1, Int2) ->
    max(Int1, Int2).

-spec merge_scores(list(score())) -> list(score()).
merge_scores(Scores) ->
    merge_scores(Scores, #{}).

-spec merge_scores(list(score()), #{ercoin_sig:address() => score()}) -> list(score()).
merge_scores([], Acc) ->
    maps:values(Acc);
merge_scores([Score={SatoshisBurnt, {Address, LockedFor, ValidatorAddress}}|TailScores], Acc) ->
    merge_scores(
      TailScores,
      maps:update_with(
        Address,
        fun ({OldSatoshisBurnt, {_, OldLockedFor, OldValidatorAddress}}) ->
                NewValidatorAddress =
                    case LockedFor > OldLockedFor of
                        true ->
                            ValidatorAddress;
                        false ->
                            %% We process scores from newest outputs first, which should take precedence if lock times are equal.
                            OldValidatorAddress
                    end,
                {OldSatoshisBurnt + SatoshisBurnt, {Address, max_on_maybe(LockedFor, OldLockedFor), NewValidatorAddress}}
        end,
        Score,
        Acc)).

-spec initial_distribution(string(), boolean()) -> ercoin_account:accounts().
initial_distribution(BurntFilename, BeLiberal) ->
    {ok, BurntFile} = file:open(BurntFilename, [read]),
    Scores = extract_from_file_lines(BurntFile, fun (Line) -> line_to_score(Line, BeLiberal) end),
    MergedScores = merge_scores(Scores),
    NonDustScores =
        lists:filter(
          fun ({Amount, _}) -> Amount >= ?BLK_DUST_THRESHOLD end,
          MergedScores),
    scores_to_distribution(NonDustScores).

-spec initial_data_without_votes(ercoin_account:accounts()) -> data().
initial_data_without_votes(Accounts) ->
    Data1 =
        #data{
           epoch_length=3600*24*7,
           epoch_stage=beginning,
           last_epoch_end=?GENESIS_TIME,
           previous_timestamp=?GENESIS_TIME,
           timestamp=?GENESIS_TIME,
           accounts=Accounts,
           entropy_fun=fun ercoin_entropy:reliable_entropy/1},
    Validators = ercoin_validators:draw(Data1#data{timestamp=?DISTRIBUTION_END + 120}),
    Data1#data{validators=Validators}.

-spec line_to_vote_tx(string()) -> datum:option(vote_tx()).
line_to_vote_tx(Line) ->
    [_, _, BlockTimestampStr, _, _, _, ScriptHex|_] = string:split(Line, ",", all),
    BlockTimestamp = list_to_integer(BlockTimestampStr),
    case BlockTimestamp >= ?DISTRIBUTION_END + 1 andalso BlockTimestamp =< ?GENESIS_TIME - 3600 * 24 of
        true ->
            case payload_from_script_hex(ScriptHex) of
                undefined ->
                    fail("No payload.");
                Payload ->
                    case ercoin_tx:deserialize(Payload) of
                        VoteTx=#vote_tx{} ->
                            unit(VoteTx);
                        _ ->
                            fail("Not a vote tx.")
                    end
            end;
        false ->
            fail("Invalid block timestamp.")
    end.

-spec initial_data(string()) -> data().
initial_data(BurntFilename) ->
    {ok, VotesFile} = file:open(BurntFilename, [read]),
    VoteTxs = lists:reverse(extract_from_file_lines(VotesFile, fun line_to_vote_tx/1)),
    AccountsOriginal = initial_distribution(BurntFilename, false),
    Accounts = initial_distribution(BurntFilename, true),
    DataWithoutVotesStub = initial_data_without_votes(AccountsOriginal),
    DataWithoutVotes = DataWithoutVotesStub#data{accounts=Accounts},
    lists:foldl(
      fun (VoteTx, DataAcc) ->
              case ercoin_tx:error_code(VoteTx, DataAcc) of
                  ?OK ->
                      ercoin_tx:apply(VoteTx, DataAcc);
                  _ ->
                      DataAcc
              end
      end,
      DataWithoutVotes,
      VoteTxs).

-spec testnet_data() -> data().
%% @doc Similar to {@link testnet_data/1}, but data is read from stdin, assuming to be in script format.
testnet_data() ->
    {ok, Exprs, _} = io:parse_erl_exprs("Enter options for testnet data: "),
    {value, Opts, _} = erl_eval:exprs(Exprs, erl_eval:new_bindings()),
    testnet_data(Opts).

-spec testnet_data(map() | file:filename()) -> data().
%% @doc Construct testnet data using options provided directly as a map or in a script file.
%% In case of missing validators, we try to read $ERCOIN_HOME/config/priv_validator_key.json.
%% @see ercoin_data:construct/1
testnet_data(FName) when not is_map(FName) ->
    {ok, Opts} = file:script(FName),
    testnet_data(Opts);
testnet_data(DataOpts) ->
    case DataOpts of
        #{accounts_opts := AccountsOpts} ->
            %%% If someone provided a locked account without a validator, then maybe he wants to use his own validator.
            %%% We try to read this validator’s public key in advance, but don’t require it to be present until it is really needed.
            MaybeLocalValidatorPKB64 =
                case file:read_file([ercoin_persistence:data_dir(), "/config/priv_validator_key.json"]) of
                    {ok, PrivValidatorJSON} ->
                        #{<<"pub_key">> := #{<<"value">> := ValidatorPKB64}} = jiffy:decode(PrivValidatorJSON, [return_maps]),
                        ValidatorPKB64;
                    _ ->
                        none
                end,
            NewAccountsOpts =
                lists:map(
                  fun (AOpts) ->
                          case (maps:get(locked_until, AOpts, none) =/= none
                                orelse maps:get(locked_for, AOpts, none) =/= none)
                              andalso maps:get(validator_pk, AOpts, none) =:= none of
                              true ->
                                  %%% Now we ensure that the validator has been fetched successfully.
                                  true = MaybeLocalValidatorPKB64 =/= none,
                                  AOpts#{validator_pk => MaybeLocalValidatorPKB64};
                              false ->
                                  AOpts
                          end
                  end,
                  AccountsOpts),
            ercoin_data:construct(DataOpts#{accounts_opts := NewAccountsOpts});
        _ ->
            ercoin_data:construct(DataOpts)
    end.
