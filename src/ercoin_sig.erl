%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_sig).

-compile({parse_transform, category}).

-export(
   [non_malleable_part/1,
    unpack_address/1,
    verify_and_unpack_address/2,
    verify_subsigs/2]).

-import(
   datum_cat_option,
   [fail/1,
    unit/1]).

-export_type(
   [msg/0,
    sig/0]).

-type pk() :: <<_:256>>.
-type subsig() :: <<_:512>>.
-type address() :: <<_:256>>.
-type sig() :: binary().
-type msg() :: binary().

-spec unpack_address(sig()) -> datum:option(address()).
unpack_address(<<SubsigCount, Rest/binary>>) ->
    PKsSize = SubsigCount * 32,
    SigsSize = SubsigCount * 64,
    PathBinSize = byte_size(Rest) - PKsSize - SigsSize,
    case Rest of
        <<PKs:PKsSize/binary, PathBin:PathBinSize/binary, _/binary>> ->
            PKsCombined =
                case SubsigCount of
                    1 ->
                        unit(PKs);
                    _ ->
                        unit(crypto:hash(sha256, PKs))
                end,
            case merkle_proofs:'deserialize-path'(PathBin) of
                none ->
                    fail("Invalid path format");
                Path ->
                    merkle_proofs:fold({PKsCombined, Path})
            end;
        _ ->
            fail("Invalid format")
    end;
unpack_address(_) ->
    fail("Invalid format").

-spec non_malleable_part(sig()) -> binary().
non_malleable_part(Sig= <<SubsigCount, _/binary>>) ->
    binary:part(Sig, 0, byte_size(Sig) - SubsigCount * 64).

-spec verify_subsigs(sig(), msg()) -> boolean().
verify_subsigs(<<SubsigCount, Rest/binary>>, Msg) when SubsigCount > 0 ->
    PKsSize = SubsigCount * 32,
    SigsSize = SubsigCount * 64,
    PathSize = byte_size(Rest) - PKsSize - SigsSize,
    case Rest of
        <<PKsBin:PKsSize/binary, PathBin:PathSize/binary, SigsBin:SigsSize/binary>> ->
            PKs = [PK || <<PK:32/binary>> <= PKsBin],
            Sigs = [Sig || <<Sig:64/binary>> <= SigsBin],
            SignedMsg = <<Msg/binary, SubsigCount, PKsBin/binary, PathBin/binary>>,
            verify_subsigs_1(lists:zip(PKs, Sigs), SignedMsg);
        _ ->
            false
    end;
verify_subsigs(_, Msg) when is_binary(Msg) ->
    false.

-spec verify_subsigs_1(list({pk(), subsig()}), binary()) -> boolean().
verify_subsigs_1([], _) ->
    true;
verify_subsigs_1([{PK, Sig}|Rest], Msg) ->
    case verify_ed25519(Sig, Msg, PK) of
        true ->
            verify_subsigs_1(Rest, Msg);
        false ->
            false
    end.

-spec verify_and_unpack_address(binary(), msg()) -> datum:option(address()).
verify_and_unpack_address(MaybeSig, Msg) ->
    [option ||
        Address <- unpack_address(MaybeSig),
        cats:require(
          verify_subsigs(MaybeSig, Msg),
          Address,
          "Invalid subsigs.")].

-spec verify_ed25519(subsig(), binary(), pk()) -> boolean().
verify_ed25519(Signature, Msg, PK) ->
    case enacl:sign_verify_detached(Signature, Msg, PK) of
        {ok, _} ->
            true;
        {error, failed_verification} ->
            false
    end.
