%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_query).

-include_lib("abci_server/include/abci.hrl").
-include_lib("include/ercoin.hrl").

-export(
   [perform/2]).

-spec perform(abci:'tendermint.abci.types.RequestQuery'(), data()) -> abci:'tendermint.abci.types.ResponseQuery'().
perform(#'tendermint.abci.types.RequestQuery'{path=Path, data=QueryData}, Data=#data{accounts=Accounts}) ->
    {Code, ResponseValue, Error} =
        case iolist_to_binary(Path) of
            <<"account">> ->
                case gb_merkle_trees:lookup(QueryData, Accounts) of
                    none ->
                        {?NOT_FOUND, <<>>, "Account not found."};
                    AccountBin ->
                        {?OK, AccountBin, []}
                end;
            <<"fees">> ->
                {?OK, ercoin_fee:serialize(ercoin_fee:fees(Data)), []};
            _ ->
                {?BAD_REQUEST, <<>>, []}
        end,
    #'tendermint.abci.types.ResponseQuery'{code=Code, value=ResponseValue, log=Error}.
