%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_timestamp).

-export_type(
   [timestamp/0]).

-export(
   [from_iso/1,
    from_protobuf/1,
    now/0,
    now/1,
    to_iso/1,
    to_protobuf/1]).

-include_lib("abci_server/include/abci.hrl").

%% @type timestamp() = 0..4294967295. POSIX timestamp in seconds.
-type timestamp() :: 0..4294967295.

-spec to_iso(timestamp()) -> binary().
to_iso(Timestamp) ->
    'Elixir.DateTime':to_iso8601(
      'Elixir.DateTime':'from_unix!'(Timestamp, second),
      extended).

-spec now() -> timestamp().
now() ->
    'Elixir.DateTime':to_unix('Elixir.DateTime':utc_now(), second).

-spec now(any()) -> timestamp().
now(_) ->
    ?MODULE:now().

-spec from_iso(binary()) -> timestamp().
from_iso(ISO) ->
    {ok, DateTime, 0} = 'Elixir.DateTime':'from_iso8601'(ISO),
    'Elixir.DateTime':to_unix(DateTime, second).

-spec from_protobuf(#'google.protobuf.Timestamp'{}) -> timestamp().
from_protobuf(#'google.protobuf.Timestamp'{seconds=Secs}) ->
    Secs.

-spec to_protobuf(timestamp()) -> #'google.protobuf.Timestamp'{}.
to_protobuf(Timestamp) ->
    #'google.protobuf.Timestamp'{
       seconds=Timestamp,
       nanos=0}.
